from django.db import models

# Create your models here.
from django.urls import reverse

class Category(models.Model):
    name = models.CharField(verbose_name="noms", max_length=200,
                            db_index=True)
    slug = models.SlugField(max_length=200,
                            unique=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'catégorie'
        verbose_name_plural = 'catégories'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
            return reverse('shop:product_list_by_category',
                           args=[self.slug])


class Product(models.Model):
    category = models.ForeignKey(Category,
                                 related_name='products',
                                 on_delete=models.CASCADE)
    name = models.CharField(verbose_name="produit", max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, db_index=True)
    image = models.ImageField(upload_to='products/%Y/%m/%d',
                              blank=True)
    description = models.TextField(verbose_name="description", blank=True)
    price = models.DecimalField(verbose_name="prix", max_digits=10, decimal_places=2)
    available = models.BooleanField(verbose_name="disponible", default=True)
    created = models.DateTimeField(verbose_name="établi", auto_now_add=True)
    updated = models.DateTimeField(verbose_name="mis à jour", auto_now=True)

    class Meta:
        verbose_name="produit"
        ordering = ('name',)
        index_together = (('id', 'slug'),)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
            return reverse('shop:product_detail',
                           args=[self.id, self.slug])