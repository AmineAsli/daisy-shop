from django.db import models

# Create your models here.
from django.db import models
from shop.models import Product


class Order(models.Model):
    first_name = models.CharField(verbose_name="prénom", max_length=50) 
    last_name = models.CharField(verbose_name="nom", max_length=50)
    email = models.EmailField()
    address = models.CharField(verbose_name="adresse", max_length=250)
    postal_code = models.CharField(verbose_name="code postal", max_length=20)
    city = models.CharField(verbose_name="ville", max_length=100)
    created = models.DateTimeField(verbose_name="établi", auto_now_add=True)
    updated = models.DateTimeField(verbose_name="mis à jour", auto_now=True)
    paid = models.BooleanField(verbose_name="payé", default=False)
    braintree_id = models.CharField(max_length=150, blank=True)

    class Meta:
        verbose_name = "commande"
        ordering = ('-created',)
        

    def __str__(self):
        return 'Commande {}'.format(self.id)

    def get_total_cost(self):
        return sum(item.get_cost() for item in self.items.all())


class OrderItem(models.Model):
    order = models.ForeignKey(Order,
                              related_name='items',
                              on_delete=models.CASCADE)
    product = models.ForeignKey(Product,
                                related_name='order_items',
                                on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    quantity = models.PositiveIntegerField(default=1)

    class Meta:
        verbose_name = "article"

    def __str__(self):
        return '{}'.format(self.id)

    def get_cost(self):
        return self.price * self.quantity